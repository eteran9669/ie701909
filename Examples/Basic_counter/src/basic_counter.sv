//Coder: Enrique Teran  
//Date: Jan 25 2021


//FIXME ADD GUARD BAND

import basic_counter_pkg::*;

module basic_counter(
input logic     clk,
input logic     rst_n,
input logic     en,
output data_t   count

);

data_t count_aux;
data_t count_nx;
//Code the next value with always_comb
always_comb begin
    count_nx  = count_aux + 'd1;
end
//this is pipo register
always_ff@(posedge clk, negedge rst_n)begin
if(!rst_n)
    count_aux <= '0;
else if(en)
    count_aux <= count_nx;
end

assign count =count_aux;
endmodule 