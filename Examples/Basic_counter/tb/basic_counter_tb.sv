`timescale 1ns / 1ps

module basic_counter_tb();
import basic_counter_pkg::*;
localparam PERIOD = 8;

//signal declaration
bit clk;
bit rst_n;
logic en;
data_t count;

//Instance of unit under test
basic_counter uut_dut(
	.clk (clk),
	.rst_n(rst_n),
	.en   (en),
	.count(count)
	);
//es un hilo de ejecucion se pueden levantar tantos hilos como quiera 
initial begin
    clk      = '0;
    rst_n    = '0;
    en       = 'd1;
    #(PERIOD) en= $random();
end


always begin 
    #(PERIOD/2) clk <= ~clk;
end

endmodule