class tester_fifo; 

localparam DW        = 16;
localparam PERIOD    = 4; // Minimum value 4
localparam TRUE      = 1'b1;
localparam FALSE     = 1'b0;

typedef logic [DW-1:0]       data_t;

data_t  q_a[$];
data_t  q_b[$];


virtual fifo_if itf;



function new(virtual fifo_if.tstr t);
	itf = t;
endfunction

// Code your tasks here

/* Injection data task FIFO */

task automatic inj_fifo_data( );
   itf.datain = 'b0;
  
   repeat (4) begin
	    itf.datain = $random;
	    end
endtask

/* Injection push task FIFO */
task automatic inj_fifo_push();
   repeat(4)begin
   
    #1 itf.push = 'b0;
	#2 itf.push = 'b1; /* Push val*/
	#2 itf.push = 'b0;
	end
endtask

/* Injection pop task FIFO */
task automatic inj_fifo_pop();
     repeat(4)begin
    #1 itf.pop = 'b0;
	#2 itf.pop = 'b1;  /* Pop val*/
	#2 itf.pop = 'b0;
	end
endtask


task automatic review_task();

/*Queues for review task*/
	q_a[$];
	q_b[$];
endtask



endtask 





endclass
