interface fifo_if(
	input clk
);
import sdp_sc_ram_pkg::*;	

data_t 	datain;
bit 	push;
bit 	pop;

bit 	rst;
data_t  dataout;
bit 	full;
bit 	empty;

modport tstr (

output	datain,
output	push,
output	pop,
input	clk,
input	rst,
input	dataout,
input	full

);

modport fifo (

input	datain,
input	push,
input	pop,
input	clk,
input	rst,
output	dataout,
output	full

);

endinterface