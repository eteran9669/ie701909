`timescale 1ns / 1ps
`include "tester.svh";
module fifo_wrapper(
// Clock signal
input bit clk,
// reset signal
input bit rst_n,
fifo_if.fifo fifo_itf
);

FIFO dut(
    .clk         ( clk          )       
   ,.rst         ( rst_n        )        
   ,.data_input  ( fifo_itf.datain  )               
   ,.push        ( fifo_itf.push    )         
   ,.pop         ( fifo_itf.pop     )        
   ,.data_out    ( fifo_itf.dataout )             
   ,.full        ( fifo_itf.full    )         
  // ,.empty       ( fifo_itf.empty   ) 
);

endmodule

module tb_fifo ();



bit clk;
bit rst_n;


tester_fifo t;


fifo_if fifo_itf (
   .clk(clk)
);

//Instance of the DUT
fifo_wrapper dut(
.clk        (  clk            ),
.rst_n      (  rst_n          ),
.fifo_itf   (  fifo_itf.fifo  )
);

initial begin
   t = new(fifo_itf);
   clk         = '0;
   rst_n       = 'd1;
   # 2 rst_n   = 'd0;
   # 2 rst_n   = 'd1;
   // Call your task/tasks for injecting data and push
  

      fork
         t.inj_fifo_data();
         t.inj_fifo_push();

      join

end

initial begin
   t.inj_fifo_pop();
   t.review_task(); 


end


always begin
   #1 clk <= ~clk;
end

endmodule
