// Coder:       Fernanda & Omar
// Date:        November 2020
// Name:        fifo_wptr.sv
// Description: This is the module for the write pointer

module fifo_wptr
import sdp_sc_ram_pkg::*;	
#(parameter DW = 4)
(
	input bit clk,
	input bit rst,
	input bit w_en,
	
	output logic [DW-1:0] waddr,
	output logic [DW-1:0] wptr,
	output bit w_full
	);

logic [DW-1:0] ptr_input,ff_output;
logic [W_DEPTH:0] w_ptr;	

	wr_ptr
	pointer
	(
		.clk(clk),
		.rst(rst),
		.en(w_en),
		.address(w_ptr)
	);

	register_flip_flop
	#(.DW(DW))
	write_flipflop
	(
		.clk(clk),
		.rst(rst),
		.D_input(ptr_input),
		.Q_output(ff_output)
		);
	
	
endmodule	