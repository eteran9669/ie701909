//Coder:          DSc Abisai Ramirez Perez
//Date:           03/31/2019
//Name:           sdp_sc_ram.sv
//Description:    This is the HDL of a single dual-port dual-clock random access memory. 

`ifndef SDP_SC_RAM_SV
    `define SDP_SC_RAM_SV
module sdp_sc_ram 
import sdp_sc_ram_pkg::*;
(
// Core clock a
input bit   clk,
input logic  we,
input data_t  data,
input waddr_t  wr_addr,
output data_t  rd_data,
input addr_t  rd_addr
// Memory interface
);

// Declare a RAM variable 
data_t  ram [W_DEPTH-1:0];

always_ff@(posedge clk) begin
if(we)
    ram[wr_addr[W_ADDR-1:0]] <= data;

rd_data <= ram [rd_addr];
end

endmodule
`endif

