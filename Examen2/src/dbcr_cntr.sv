//Author: 		Sabina Bruce & Omar Anguiano & Fernanda Munoz
//Date:   		September 20, 2020
//Name: 	 	dbcr_cntr.sv



module dbcr_cntr #(
parameter DW=8
)(
input           clk,
input           rst_n,
input           Din,
output [DW-1:0] count
	);
	
logic one_shot;
	
dbcr_top dbcr(
    .clk        (clk        ), 
    .rst_n      (rst_n      ), 
    .Din        (Din        ), 
    .one_shot   (one_shot   )
	);
	
bin_counter #(
    .DW(DW)
    ) cntr (
    .clk   (clk     ),
    .rst   (rst_n   ),
    .enb   (one_shot),
    .count (count   )  );
	
	
	
endmodule
