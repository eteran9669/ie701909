// Coder:       Fer & Omar 
// Date:        November 2020
// Name:        rd_compare.sv
// Description: This is a compare for the read


`ifndef RD_COMPARATOR_SV
    `define RD_COMPARATOR_SV

module rd_comparator
#(
	parameter DW = 4	
)	
(
	input logic [DW-1:0] comp_in,
	output logic [DW-1:0] comp_out
	);
	
logic[DW-1:0] temp;
	
always_comb begin
	temp = comp_in[DW-1];
end

endmodule 
`endif
