// Coder:  Enrique Teran 
// Date:   31/01/2021
// Name:  displays_module.sv

`ifndef DISPLAYS_MODULE_SV
    `define DISPLAYS_MODULE_SV

import bin_decoder_pkg::*;

module displays_module(

input output_data_t  decoded_data,
output dis_t        display_value

);

always @(decoded_data)
 assign display_value = (decoded_data==4'b0000) ? 7'b1000000:  //0 

                        (decoded_data==4'b0001) ? 7'b1111001: //1 

                        (decoded_data==4'b0010) ? 7'b0100100: //2 

                        (decoded_data==4'b0011) ? 7'b0110000: //3 

                        (decoded_data==4'b0100) ? 7'b0011001: //4 

                        (decoded_data==4'b0101) ? 7'b0010010: //5 

                        (decoded_data==4'b0110) ? 7'b0000010: //6 

                        (decoded_data==4'b0111) ? 7'b1111000: //7 

                        (decoded_data==4'b1000) ? 7'b0000000: //8 

                        (decoded_data==4'b1001) ? 7'b0011000: //9 
                        7'b0111111;

 


endmodule

`endif 