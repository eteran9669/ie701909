// Coder:  Enrique Teran 
// Date:   31/01/2021
// Name:   bcd_TOP.sv

`ifndef bcd_TOP_SV
    `define bcd_TOP_SV
import bin_decoder_pkg::*;
module bcd_TOP(
 
    input input_data_t  data,   //switches input
    output sign_t       sign,  //sign output
    output dis_t   disp_cent,
    output dis_t    disp_dec,
    output dis_t   disp_unit
  
);


//WIRES
logic [3:0]  cent_wire;
logic [3:0]   dec_wire;
logic [3:0] units_wire;


bin_decoder_module i_bin_decoder_module(

    .in_a(data),
    
    .o_cent(cent_wire),
    .o_dec(dec_wire),
    .o_unit(units_wire),
    .o_sign(sign)
);

displays_module display_cent
(
    .decoded_data(cent_wire),
    .display_value(disp_cent)
);

displays_module  display_dec
(
    .decoded_data(dec_wire),
    .display_value(disp_dec)
);

displays_module display_units
(
    .decoded_data(units_wire),
    .display_value(disp_unit)
);

endmodule

`endif 