// Coder:  Enrique Teran 
// Date:   31/01/2021
// Name:   Bin_decoder_pkg.sv

`ifndef BIN_DECODER_PKG_SV
    `define BIN_DECODER_PKG_SV


package bin_decoder_pkg;


localparam W_DW                     = 8;
localparam O_DW                     = 4;
localparam DIS_DW                   = 7;
typedef logic [W_DW-1:0]   input_data_t;
typedef logic [O_DW-1:0]  output_data_t;             
typedef logic                    sign_t;
typedef logic [DIS_DW-1:0]        dis_t;


endpackage 
`endif 