// Coder:  Enrique Teran 
// Date:   31/01/2021
// Name:   bin_decoder_module.sv

`ifndef BIN_DECODER_MODULE_SV
    `define BIN_DECODER_MODULE_SV

import bin_decoder_pkg::*;


module bin_decoder_module(

    input  input_data_t         in_a,
    output sign_t               o_sign,
    output output_data_t        o_cent,
    output output_data_t        o_dec,
    output output_data_t        o_unit
);


//---------------------------------
//       Binary decoder
//---------------------------------


dis_t         temp_data;
output_data_t temp_cent;
output_data_t  temp_dec;
output_data_t    temp_u;
int   dec_data;
always_comb begin: bin2dec_o

temp_data = (in_a[7] == 0) ? in_a[6:0]: (~in_a + 1'b1);
dec_data = temp_data;

temp_cent = (dec_data/'d100);
temp_dec  = ((dec_data%'d100)/'d10);
temp_u    = ((dec_data%'d100)%'d10);

end 

assign o_sign = in_a[7];
assign o_cent = temp_cent;
assign o_dec  = temp_dec;
assign o_unit = temp_u;

endmodule

`endif 