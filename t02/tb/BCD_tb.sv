`timescale 1ns / 1ps

module BCD_tb();
import bin_decoder_pkg::*;

bit clk;
logic [7:0]   switches_sim;
logic [6:0]    o_disp_cent; 
logic [6:0]    o_disp_dec;
logic [6:0]    o_disp_unit;
logic               sign;


bcd_TOP uut_dut(

    .data(switches_sim),
    .disp_cent(o_disp_cent),
    .disp_dec(o_disp_dec),
    .disp_unit(o_disp_unit),
    .sign(sign)
);



initial begin
	clk = 0;                                //inicializa el clock en 0
	switches_sim = 8'b00000000;             //inicializa el bus de entrada en 0

	repeat(256) begin
	@(posedge clk);
	
	switches_sim = switches_sim + 1'b1;     //incrementa el valor del bus de entrada binario
	end
	$stop;
end

always begin
#1 clk <= ~clk;
end
endmodule