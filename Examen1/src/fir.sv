//-------------------------------------------
// Coder:       Enrique Terán Quintanilla 
// Name:        fir.sv
// Description: fir module for exam 1
//-------------------------------------------

module fir
#(
	parameter DATALENGHT = 5,
	/*Coeficientes del Filtro*/
	parameter b0 = 1,
	parameter b1 = -1,
	parameter b2=  1,
	parameter b3 = -1,
	parameter b4 = 1

)
(		input logic clk,
		input logic rst,
		input logic pipo_en,
		input [DATALENGHT-1:0] data_i,
		output  [2*DATALENGHT:0] data_o
);

/*WIRES*/
logic [DATALENGHT-1:0]  z0_o_w;
logic [DATALENGHT-1:0]  z1_o_w;
logic [DATALENGHT-1:0]  z2_o_w;
logic [DATALENGHT-1:0]  z3_o_w;
logic [DATALENGHT-1:0]  z4_o_w;
logic signed[2*DATALENGHT:0]  m0_w;
logic signed[2*DATALENGHT:0]  m1_w;
logic signed[2*DATALENGHT:0]  m2_w;
logic signed[2*DATALENGHT:0]  m3_w;
logic signed[2*DATALENGHT:0]  m4_w;
logic signed[2*DATALENGHT:0]  s0_w;
logic signed[2*DATALENGHT:0]  s1_w;
logic signed[2*DATALENGHT:0]  s2_w;
logic signed[2*DATALENGHT:0]  s3_w;



gen_pipo z0_pipo
(
	
	.reset			(rst),
	.clk			(clk),
	.ready			(pipo_en),	
	.pipo_data_i	(data_i),
	.pipo_data_o	(z0_o_w)

);
gen_pipo z1_pipo
(
	
	.reset			(rst),
	.clk			(clk),
	.ready			(pipo_en),	
	.pipo_data_i	(z0_o_w),
	.pipo_data_o	(z1_o_w)

);
gen_pipo z2_pipo
(
	
	.reset			(rst),
	.clk			(clk),
	.ready			(pipo_en),	
	.pipo_data_i	(z1_o_w),
	.pipo_data_o	(z2_o_w)

);
gen_pipo z3_pipo
(
	
	.reset			(rst),
	.clk			(clk),
	.ready			(pipo_en),	
	.pipo_data_i	(z2_o_w),
	.pipo_data_o	(z3_o_w)

);
gen_pipo z4_pipo
(
	
	.reset			(rst),
	.clk			(clk),
	.ready			(pipo_en),	
	.pipo_data_i	(z3_o_w),
	.pipo_data_o	(z4_o_w)

);


/*OPERATIONS SUMS AND MULTIPLICATIONS*/
/*Multiplications*/
assign m0_w = z0_o_w * b0;
assign m1_w = z1_o_w * b1;
assign m2_w = z2_o_w * b2;
assign m3_w = z3_o_w * b3;
assign m4_w = z4_o_w * b4;
/*Sums*/
assign s0_w = m0_w + m1_w;
assign s1_w = s0_w + m2_w;
assign s2_w = s1_w + m3_w;
assign s3_w = s2_w + m4_w;

//Assigns
assign data_o = s3_w;

endmodule 