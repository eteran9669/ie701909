//-------------------------------------------
// Coder:       Enrique Terán Quintanilla & Joab Garcia Teruel
// Date:        14/02/2021
// Name:        gen_pipo.sv
// Description: PIPO file for MS Binary
//-------------------------------------------

`ifndef GEN_PIPO
    `define GEN_PIPO
module gen_pipo
#(
    parameter DATALENGHT = 5
)
(
    input logic               reset,
    input logic               clk,
    input logic               ready,
    input  [DATALENGHT-1:0]   pipo_data_i,
    output [DATALENGHT-1:0]   pipo_data_o
);

//Local Variables
logic [2*DATALENGHT:0] pipo_temp_data;   //Temp variable por passing the input data or 0's depends if ready signal or rst is active

always_ff @( posedge clk, negedge reset ) begin : pipo_main  // positive edge of the clock and negative edge of the reset signal 

    if(!reset)  // !reset because reset signal is active in low state
    begin
     pipo_temp_data <= '0;  //Logic if reset is pushed not pass any data 0's assigned to output
    end 
    else if (ready) // if ready signal is active from the FSM 
    begin
      pipo_temp_data <= pipo_data_i;  //Logic if ready signal is enable pass input data to output data
    end

end :pipo_main 

assign pipo_data_o = pipo_temp_data;  // Pass value of temp variable to output signal 

endmodule 

`endif