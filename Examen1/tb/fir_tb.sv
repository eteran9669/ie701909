`timescale 1ns / 1ps 

module fir_tb;

parameter DATALENGHT = 5;

bit 	clk;
bit 	rst;
bit 	pipo_en;
logic 	[DATALENGHT-1:0] data_i;
logic 	[2*DATALENGHT:0] data_o;

fir fir_dut
(
	.clk   		(clk),
	.rst 		(rst),
	.pipo_en	(pipo_en),
	.data_i		(data_i),
	.data_o		(data_o)
);

initial begin
	clk = 1;
	#0.1 rst = 0; //reset enable
	pipo_en = 0;  //pipo_en disable	
	data_i = 0;	  // input in 0	
	#2 rst = 1;	  //reset disable
	pipo_en = 1;  //pipo enable	
	data_i = 0;	  	
	#14 data_i = 1;
	#2  data_i = 0;
	#12 rst = 0;  //reset enable
	#20 $finish;
end

always begin
	#1 clk <= ~clk;
end

endmodule