//------------------------------------------------------------
// Coder:       Enrique Terán Quintanilla 
// Date:        21/02/2021
// Name:        clk_div.sv
// Description: Clock Divider module
//------------------------------------------------------------

`ifndef CLK_DIV_SV
    `define CLK_DIV_SV

module clk_div #(

    parameter REFERENCE_CLOCK = 50_000_000,
    parameter FRECUENCY       = 5_000_000,
    parameter FRECUENCY_DIVIDER = REFERENCE_CLOCK/(2*FRECUENCY)
   

)
(
    input logic     clk_fpga,       //FPGA CLOCK
    input logic     rst,            //RESET SWITCH
    output logic    clk_divided     //CLOCK DIVIDED
);

//Local variables
logic [29:0]counter;  //Counter for clock divider Big register for future implementations
logic max_val = FRECUENCY_DIVIDER - 1'b1;  //Max value for the counter

always_ff @(posedge clk_fpga, negedge rst)
begin
    if(!rst)               //If reset
    begin
       clk_divided <= '0;  //Set output value to 0 
       counter <= '0;      //Set counter value to 0
    end
    else if(counter == max_val)  //If counter reach max_value
    begin
        counter <= '0;                      //Reset counter value 
        clk_divided <= ~(clk_divided);      //The value is negated to switch the value 
    end
    else
    begin
        counter <= counter + 1'b1; //Counter value increment 1 value
    end
    
end
endmodule

`endif