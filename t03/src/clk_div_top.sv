//------------------------------------------------------------
// Coder:       Enrique Terán Quintanilla 
// Date:        21/02/2021
// Name:        clk_div_top.sv
// Description: Clock Divider TOP module
//------------------------------------------------------------

module clk_div_top #(

    parameter REFERENCE_CLOCK = 50_000_000,
    parameter FRECUENCY       = 5_000_000,
    parameter FRECUENCY_DIVIDER = REFERENCE_CLOCK/(2*FRECUENCY)
   
)
(
    input logic     clk_fpga_i,     //FPGA CLOCK 
	input logic     rst_i,          //RESET SWITCH
    output logic    clk_divided_o   //OUTPUT 
);

clk_div clock_divider_top
(
    .clk_fpga       (clk_fpga_i),
    .rst            (rst_i),
    .clk_divided    (clk_divided_o)

);
endmodule