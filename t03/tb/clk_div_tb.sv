`timescale 1ns / 1ps
module clk_div_tb();


    logic     clk_fpga_i;     //FPGA CLOCK 
    logic     rst_i;          //RESET SWITCH
    logic    clk_divided_o;   //OUTPUT 


    clk_div_top uut
    (
        .clk_fpga_i(clk_fpga_i),
	    .rst_i(rst_i),
	    .clk_divided_o(clk_divided_o)
    );


    initial 
    begin

        
        clk_fpga_i = 0;
        rst_i = 0;  
        #1 rst_i = 1; 


        $stop;
    end

    always begin
    #1 clk_fpga_i <= ~(clk_fpga_i);
    end

endmodule