if [file exists work] {vdel -all}
vlib work
vlog +define+USIGNED_OP -f files.f
onbreak {resume}
set NoQuitOnFinish 1

vsim -voptargs=+acc work.clk_div_tb 
do wave.do
run 150ns
